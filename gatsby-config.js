let activeEnv =
  process.env.GATSBY_ACTIVE_ENV || process.env.NODE_ENV || "development"
console.log(`Using environment config: .env.${activeEnv}`)
require("dotenv").config({
  path: `.env.${activeEnv}`,
})

module.exports = {
  pathPrefix: process.env.PATH_PREFIX,
  siteMetadata: {
    title: process.env.TITLE,
    description: process.env.DESCRIPTION,
    author: `Florian HESLOUIN, credits: Kyle Pennelle`,
  },
  plugins: [
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    `gatsby-plugin-material-ui`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-intl`,
      options: {
        // language JSON resource path
        path: `${__dirname}/src/intl`,
        // supported language
        languages: [`en`, `fr`, `ar`],
        // language file path
        defaultLanguage: `en`,
        // option to redirect to `/ko` when connecting `/`
        redirect: true,
      }
    },
    {
      resolve: "gatsby-plugin-remote-images",
      options: {
        nodeType: "GoogleSpreadsheetExport",
        imagePath: "thumbnail",
        name: 'optimized_thumbnail',
      }
    },
    {
      resolve: "gatsby-plugin-remote-images",
      options: {
        nodeType: "GoogleSpreadsheetExport",
        imagePath: "locationImage", 
        name: 'optimized_location_image',
      }
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/static/img`,
        name: 'uploads',
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/pages`,
        name: 'pages',
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: process.env.TITLE,
        short_name: process.env.TITLE,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `standalone`,
        
      },
    },
    {
      resolve: "gatsby-source-google-spreadsheet",
      options: {
        spreadsheetId: process.env.SPREADSHEET_ID,
        typePrefix: "GoogleSpreadsheet",
        credentials: {
          client_email: process.env.CLIENT_EMAIL,
          private_key: process.env.PRIVATE_KEY.replace(/\\n/g, '\n'),
        },
        filterNode: () => true,
        mapNode: node => node
      }
    },{
      resolve: `gatsby-plugin-offline`,
      options: {
        workboxConfig: {
          "globDirectory": "public/",
          "globPatterns": [
            "**/*.{html,js,json,png,jpg,webmanifest,woff,woff2,ttf,eot,css,mjs}"
          ],
          "swDest": "public/sw.js"
        },
      },
    },
    ],
  }