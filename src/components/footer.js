import React from "react";
import Typography from "@material-ui/core/Typography";
import { graphql, useStaticQuery } from "gatsby";
import Img from "gatsby-image";

export default () => {
  const data = useStaticQuery(graphql`
    query MyQuery {
      file(relativePath: { eq: "logo-bsf.jpg" }) {
        childImageSharp {
          fixed(height: 40) {
            ...GatsbyImageSharpFixed
          }
        }
      }
    }
  `)
  
  return (
    <>
    <Typography variant="caption" color="textSecondary">
        A project by
    </Typography>
    <Typography >
        <Img 
          fixed={data.file.childImageSharp.fixed}
        />
    </Typography>
    </>
  )
}